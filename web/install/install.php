<?php include "../admin/functions.php" ?>
<?php include "functions.php" ?>
<?php
if(isset($_POST['submit']))
{
	$crypt_cost = 12;
	$db_server = 'localhost';
	$admin_name = CheckSqlWithoutConnection($_POST['admin-name']);
	$admin_password = CheckSqlWithoutConnection($_POST['admin-password']);
	$admin_email = CheckSqlWithoutConnection($_POST['admin-email']);
	$admin_firstname = CheckSqlWithoutConnection($_POST['admin-firstname']);
	$admin_lastname = CheckSqlWithoutConnection($_POST['admin-lastname']);
	$db_name = CheckSqlWithoutConnection($_POST['db-name']);
	$db_user = CheckSqlWithoutConnection($_POST['db-username']);
	$db_password = CheckSqlWithoutConnection($_POST['db-password']);
	$admin_image = $_FILES['admin-image']['name'];
	$admin_image_tmp = $_FILES['admin-image']['tmp_name'];
	$admin_password = password_hash($admin_password, PASSWORD_BCRYPT, array('cost' => $crypt_cost));
	if(isset($_POST['db-server']))
	{
		$db_server = CheckSqlWithoutConnection($_POST['db-server']);
	}
	if(InstallSoftware($admin_name, $admin_password, $admin_email, $admin_firstname, $admin_lastname, $admin_image, $db_name, $db_user, $db_password))
	{
		// Succeeded. 
		CreateDbFile($db_server, $db_name, $db_user, $db_password);
		move_uploaded_file($admin_image_tmp, "../images/$admin_image");
	} else
	{
		// Failed;
	}

}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Install CMS</title>

	<!-- Bootstrap Core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="../css/blog-home.css" rel="stylesheet">
</head>

<body>
	<div class="container">

		<section id="login">
			<div class="container">
				<div class="row">
					
					<div class="form-wrap">
						<h1>Installation</h1>
						<form action="" class="form-horizontal" method="post" id="install-form" autocomplete="off" enctype="multipart/form-data">
							<div class="col-xs-3 col-xs-offset-0">
								<div class="form-group">
									<label for="admin-name">Admin name</label>
									<input type="text" required name="admin-name" id="admin-name" class="form-control" placeholder="Enter Main User Admin">
								</div>
								<div class="form-group">
									<label for="admin-password">Admin Password</label>
									<input type="password" required name="admin-password" id="admin-password" class="form-control" placeholder="Create Admin Password">
								</div>
								<div class="form-group">
									<label for="admin-pass">Admin Email</label>
									<input type="email" required name="admin-email" id="admin-email" class="form-control" placeholder="someone@somewhere.com">
								</div>
								<div class="form-group">
									<label for="admin-firstname">Admin Firstname</label>
									<input type="text" required name="admin-firstname" id="admin-firstname" class="form-control" placeholder="Firstname">
								</div>
								<div class="form-group">
									<label for="admin-lastname">Admin Lastname</label>
									<input type="text" required name="admin-lastname" id="admin-lastname" class="form-control" placeholder="Lastname">
								</div>
								<div class="form-group">
									<label for="user_image">Admin Avatar</label>
									<input type="file" class="form-control" name="admin-image">
								</div>
							</div>
							<div class="col-xs-3 col-xs-offset-1">
								<div class="form-group">
									<label for="admin-name">Server name (leave blank if installing on same machine as website)</label>
									<input type="text" required name="db-server" id="db-server" class="form-control" placeholder="Server name">
								</div>
								<div class="form-group">
									<label for="admin-name">Database Username</label>
									<input type="text" required name="db-username" id="db-username" class="form-control" placeholder="Database username">
								</div>
								<div class="form-group">
									<label for="admin-name">Database Name</label>
									<input type="text" required name="db-name" id="db-name" class="form-control" placeholder="Database name">
								</div>
								<div class="form-group">
									<label for="admin-pass">Database Password</label>
									<input type="password" required name="db-password" id="db-password" class="form-control" placeholder="Database Password">
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-xs-3 col-xs-offset-0">
								<input type="submit" name="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Install!">
							</div>
						</form>

					</div>
					<!-- /.col-xs-12 -->
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="col-lg-12">
					<p>Copyright &copy; Simon Preston <?php echo date('Y') ?></p>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</footer>

	</div>
	<!-- /.container -->

	<!-- jQuery -->
	<script src="../js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="../js/bootstrap.min.js"></script>

</body>

</html>
