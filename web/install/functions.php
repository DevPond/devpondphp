<?php
function InstallSoftware($admin_name, $admin_password, $admin_email, $admin_firstname, $admin_lastname, 
	$admin_image, $db_name, $db_user, $db_password, $db_server)
{
	$db['db_host'] = 'localhost';
	$db['db_user'] = $db_user;
	$db['db_pass'] = $db_pass;
	$db['db_name'] = $db_name;

	if(isset($db_server))
	{
		$db['db_host'] = $db_server;
	}

	foreach ($db as $key => $value) {
		define(strtoupper($key), $value);
	}
	$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if(!$connection)
	{
		die("Error connecting to the database: " . mysqli_error($connection));
	}
	$install_query = GetInstallQuery();
	try {
		$result = mysqli_query($connection, $query);
		return true;
	} catch (Exception $e) {
		return false;
	}
	return false;
}
function CreateDbFile($db_host, $db_name, $db_user, $db_pass)
{
	$db_file = fopen("../includes/db_test.php", "w");

	$file_line = "<?php\n\n";
	$file_line .= "\$db['db_host'] = '{$db_host}';\n";
	$file_line .= "\$db['db_user'] = '{$db_user}';\n";
	$file_line .= "\$db['db_pass'] = '{$db_pass}';\n";
	$file_line .= "\$db['db_name'] = '{$db_name}';\n";
	$file_line .= "foreach (\$db as \$key => \$value) {\n";
	$file_line .= "	define(strtoupper(\$key), \$value)\n";
	$file_line .= "}\n";
	$file_line .= "\$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);\n";
	$file_line .= "if(!\$connection)\n";
	$file_line .= "{\n";
	$file_line .= "	die('Error connecting to the database: ' . mysqli_error(\$connection));\n";
	$file_line .= "}\n";
	$file_line .= "?>";

	fwrite($db_file, $file_line);
	fclose($db_file);
}
function CheckSqlWithoutConnection($string)
{
    return mysql_real_escape_string(trim($string));
}

function GetInstallQuery()
{
	$query = "SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';";
	$query .= "SET time_zone = '+00:00';";
	$query .= "CREATE DATABASE IF NOT EXISTS '$database_name' DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;";
	$query .= "USE '$database_name';";
	$query .= "CREATE TABLE `categories` (";
	$query .= "  `cat_id` int(3) NOT NULL,";
	$query .= "  `cat_title` varchar(255) NOT NULL";
	$query .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	$query .= "INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES";
	$query .= "(1, 'Bootstrap'),";
	$query .= "(2, 'Javascript'),";
	$query .= "(3, 'Java'),";
	$query .= "(4, 'PHP'),";
	$query .= "(10, 'OOP');";
	$query .= "CREATE TABLE `comments` (";
	$query .= "  `comment_id` int(3) NOT NULL,";
	$query .= "  `comment_post_id` int(3) NOT NULL,";
	$query .= "  `comment_author` varchar(255) NOT NULL,";
	$query .= "  `comment_email` varchar(255) NOT NULL,";
	$query .= "  `comment_content` text NOT NULL,";
	$query .= "  `comment_status` varchar(255) NOT NULL,";
	$query .= "  `comment_date` date NOT NULL";
	$query .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	$query .= "CREATE TABLE `posts` (";
	$query .= "  `post_id` int(3) NOT NULL,";
	$query .= "  `post_category_id` int(3) NOT NULL,";
	$query .= "  `post_title` varchar(255) NOT NULL,";
	$query .= "  `post_author` int(4) NOT NULL,";
	$query .= "  `post_date` date NOT NULL,";
	$query .= "  `post_image` text NOT NULL,";
	$query .= "  `post_content` text NOT NULL,";
	$query .= "  `post_tags` varchar(255) NOT NULL,";
	$query .= "  `post_comment_count` int(11) NOT NULL,";
	$query .= "  `post_status` varchar(255) NOT NULL DEFAULT 'draft',";
	$query .= "  `post_views_count` int(11) NOT NULL";
	$query .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	$query .= "CREATE TABLE `roles` (";
	$query .= "  `role_id` int(11) NOT NULL,";
	$query .= "  `role_title` varchar(255) NOT NULL";
	$query .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	$query .= "INSERT INTO `roles` (`role_id`, `role_title`) VALUES";
	$query .= "(1, 'admin'),";
	$query .= "(2, 'user'),";
	$query .= "(3, 'standard'),";
	$query .= "(4, 'moderator');";
	$query .= "CREATE TABLE `users` (";
	$query .= "  `user_id` int(11) NOT NULL,";
	$query .= "  `username` varchar(255) NOT NULL,";
	$query .= "  `user_password` varchar(255) NOT NULL,";
	$query .= "  `user_firstname` varchar(255) NOT NULL,";
	$query .= "  `user_lastname` varchar(255) NOT NULL,";
	$query .= "  `user_email` varchar(255) NOT NULL,";
	$query .= "  `user_image` text NOT NULL,";
	$query .= "  `user_role` varchar(255) NOT NULL";
	$query .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	$query .= "INSERT INTO `users` (`user_id`, `username`, `user_password`, `user_firstname`, `user_lastname`, `user_email`, `user_image`, `user_role`) VALUES";
	$query .= "(1, '{$admin_name}', '${admin_password}', '{$admin_firstname}', '{$admin_lastname}', '{$admin_email}', '{$admin_image}', '1');";
	$query .= "CREATE TABLE `users_online` (";
	$query .= "  `id` int(11) NOT NULL,";
	$query .= "  `session` varchar(255) NOT NULL,";
	$query .= "  `time` int(11) NOT NULL";
	$query .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	$query .= "ALTER TABLE `categories`";
	$query .= "  ADD PRIMARY KEY (`cat_id`);";
	$query .= "ALTER TABLE `comments`";
	$query .= "  ADD PRIMARY KEY (`comment_id`);";
	$query .= "ALTER TABLE `posts`";
	$query .= "  ADD PRIMARY KEY (`post_id`);";
	$query .= "ALTER TABLE `roles`";
	$query .= "  ADD PRIMARY KEY (`role_id`);";
	$query .= "ALTER TABLE `users`";
	$query .= "  ADD PRIMARY KEY (`user_id`);";
	$query .= "ALTER TABLE `users_online`";
	$query .= "  ADD PRIMARY KEY (`id`);";
	$query .= "ALTER TABLE `categories`";
	$query .= "  MODIFY `cat_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;";
	$query .= "ALTER TABLE `comments`";
	$query .= "  MODIFY `comment_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;";
	$query .= "ALTER TABLE `posts`";
	$query .= "  MODIFY `post_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;";
	$query .= "ALTER TABLE `roles`";
	$query .= "  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;";
	$query .= "ALTER TABLE `users`";
	$query .= "  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;";
	$query .= "ALTER TABLE `users_online`";
	$query .= "  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;";

	return $query;
}


?>