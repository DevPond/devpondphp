<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">HOME</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php
                $query = "SELECT * FROM categories";
                $select_all_categories = mysqli_query($connection, $query);
                $page_name = basename($_SERVER['PHP_SELF']);

                while($row = mysqli_fetch_assoc($select_all_categories))
                {
                    $cat_title = $row['cat_title'];
                    $cat_id = $row['cat_id'];
                    $cat_class = '';
                    if(isset($_GET['category']))
                    {
                        if($_GET['category'] == $cat_id) {
                            $cat_class = "active";
                        }
                    }
                    echo "<li  class='{$cat_class}'><a href='category.php?category={$cat_id}'>{$cat_title}</a></li>";
                }
                ?>
                <li<?php if($page_name == "contact.php") {echo " class='active'";} ?>>
                <a href="contact.php">Contact</a>
            </li>
            <?php
            if(isset($_SESSION['user_role']))
            {
                if($_SESSION['user_role'] == '1')
                {
                    ?>
                    <li>
                        <a href="admin">Admin</a>
                    </li>
                    <?php 
                } ?>
                <li>
                    <a href="includes/logout.php">Logout</a>
                </li>
                <?php 
            }
            else { ?>
            <li<?php if($page_name == "registration.php") {echo " class='active'";} ?>>
            <a href="../registration.php">Register</a>
        </li> <?php } ?>
        
                    <!--
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                -->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>