<?php session_start(); ?>
<?php include "db.php" ?>
<?php include "../admin/includes/sql_statements.php" ?>
<?php include "../admin/functions.php" ?>

<?php
if(isset($_POST['login'])) 
{

	$username = strtolower($_POST['username']);
	$password = $_POST['password'];

	$username = CheckSql($username);
	$password = CheckSql($password);

	$query = "SELECT * FROM users WHERE username = '{$username}' ";
	//$query .= "AND user_password = '{$password}'";
	$select_user_query = mysqli_query($connection, $query);
	if(!$select_user_query)
	{
		die("Error logging in!". mysqli_error($connection));
	}

	$row = mysqli_fetch_array($select_user_query);
	
		$user_id = $row['user_id'];
		$user_username = $row['username'];
		$user_password = $row['user_password'];
		$user_user_firstname = $row['user_firstname'];
		$user_user_lastname = $row['user_lastname'];
		$user_user_role = $row['user_role'];
		
	

	if (password_verify($password, $user_password)) 
	{
		$_SESSION['user_id'] = $user_id;
		$_SESSION['username'] = $user_username;
		$_SESSION['firstname'] = $user_user_firstname;
		$_SESSION['lastname'] = $user_user_lastname;
		$_SESSION['user_role'] = $user_user_role;

		header("Location: ../admin/index.php");
	} else {
		header("Location: ../index.php");
	}
	

	
}

?>