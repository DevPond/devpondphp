<?php include "includes/admin_header.php" ?>

<div id="wrapper">

    <!-- Navigation -->
    <?php include "includes/admin_navigation.php" ?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <?php include "includes/page_header.php" ?>
            
            <?php
            $source = '';
            if(isset($_GET['source']))
            {
                $source = CheckSql($_GET['source']);
            }

            switch ($source) {
                case 'add_user':
                include "includes/add_user.php";
                break;
                case 'edit':
                include "includes/edit_user.php";
                break;
                
                default:
                include "includes/view_all_users.php";
                break;
            }

            ?>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

    <?php include "includes/admin_footer.php" ?>

