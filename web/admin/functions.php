<?php
if(isset($_GET['online_users']))
{
    UsersOnlineCount();
}

function RedirectToIndex()
{
    header("Location: index.php");
}
function CheckSql($string)
{
    global $connection;
    return mysqli_real_escape_string($connection, trim($string));

}
function SessionUserIsAdmin()
{
    if(isset($_SESSION['user_role']))
    {
        if($_SESSION['user_role'] = 1)
        {
            return true;
        }
        return false;
    }
    return false;
}

function confirmQuery($result, $connection) 
{
    if(!$result)
    {
        die("Error processing transaction: " . mysqli_error($connection));
        return false;
    }
    return true;
}
function insertCategories()
{
	global $connection;
	if(isset($_POST['submit']))
	{
     $ctitle = $_POST['cat_title'];
     if($ctitle == "" || empty($ctitle))
     {
         echo "This field was empty";
     } else
     {
         $query = "INSERT INTO categories(cat_title) VALUES('{$ctitle}')";

         $create_category_query = mysqli_query($connection, $query);

         if(!$create_category_query)
         {
             die("Error" . mysqli_error($connection));
         }
     }
 }
}

function findAllCategories()
{
	global $connection;

    $query = "SELECT * FROM categories";
    $select_categories = mysqli_query($connection, $query);

    while($row = mysqli_fetch_assoc($select_categories))
    {
        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
        echo "<tr>";
        echo "<td>{$cat_id}</td>";
        echo "<td>{$cat_title}</td>";
        echo "<td><a href='categories.php?delete={$cat_id}'>Delete</a></td>";
        echo "<td><a href='categories.php?edit={$cat_id}'>Edit</a></td>";
        echo "</tr>";
    }
}

function deleteCategory()
{
	global $connection;
	if(isset($_GET['delete']))
    {
        $the_cat_id = mysqli_real_escape_string($connection, $_GET['delete']);

        $query = "DELETE FROM categories WHERE cat_id = {$the_cat_id}";
        $delete_query = mysqli_query($connection, $query);
        header("Location: categories.php");
    } else {
    }
}
function deleteRecord($connection, $type, $recordId)
{
    $query = "DELETE FROM {$type}s WHERE {$type}_id = {$recordId} ";
    $delete_query = mysqli_query($connection, $query);
}
function checkAuthentication()
{
    if(!isset($_SESSION['user_role']))
    {
        header("Location: ../index.php");
    } else {
        $user_role = $_SESSION['user_role'];
        if($user_role !== 1)
        {
            header("Location: ../index.php");
        }
    }
}
function UsersOnlineCount()
{
    global $connection;
    if(isset($_GET['online_users']))
    {
        if(!$connection)
        {
            session_start();
            include("../includes/db.php");
        }
        $session = session_id();
        $time = time();
        $time_out_in_seconds = 30;
        $time_out = $time - $time_out_in_seconds;

        $query = "SELECT * FROM users_online WHERE session = '{$session}'";
        $query_users = mysqli_query($connection, $query);
        $online_count = mysqli_num_rows($query_users);

        if($online_count == NULL)
        {
            $insert_query = "INSERT INTO users_online (session, time) VALUES ('$session', '$time') ";
            $insert_query_result = mysqli_query($connection, $insert_query);
        } else {
            $update_query = "UPDATE users_online SET time = '{$time}' WHERE session = '{$session}' ";
            mysqli_query($connection, $update_query);
        }

        $users_online_query = "SELECT * FROM users_online WHERE time > '{$time_out}' ";
        $users_online_result = mysqli_query($connection, $users_online_query);

        echo mysqli_num_rows($users_online_result);
    }
}


?>