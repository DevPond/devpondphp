<?php

function GetUsersWithRoles()
{
	$query = "SELECT ";
	$query .= "user_id, ";
	$query .= "username, ";
	$query .= "user_firstname, ";
	$query .= "user_lastname, ";
	$query .= "user_email, ";
	$query .= "user_image, ";
	$query .= "r.role_title ";

	$query .= "FROM users ";
	$query .= "INNER JOIN roles as r ON users.user_role = r.role_id";

	return $query;
}

function GetUserByUsername($username)
{
	$query = "SELECT ";
	$query .= "user_id, ";
	$query .= "username, ";
	$query .= "user_firstname, ";
	$query .= "user_lastname, ";
	$query .= "user_email, ";
	$query .= "user_image, ";
	$query .= "r.role_title ";

	$query .= "FROM users ";
	$query .= "INNER JOIN roles as r ON users.user_role = r.role_id ";

	$query .= "WHERE username = '{$username}' ";

	return $query;
}
function GetRecordsCount($connection, $table)
{
	$query = "SELECT * FROM {$table}";
	$select_all_records = mysqli_query($connection, $query);
	$result = mysqli_num_rows($select_all_records);

	return $result;
}
function GetRecordsCountWhere($connection, $table, $where)
{
	$query = "SELECT * FROM {$table} WHERE {$where}";
	$select_all_records = mysqli_query($connection, $query);
	$result = mysqli_num_rows($select_all_records);

	return $result;
}
function ExecuteQuery($query)
{
	global $connection;
	return mysqli_query($connection, $query);
}
function DeleteRecordsWhere($connection, $table, $where)
{
	$query = "DELETE FROM {$table} WHERE {$where}";
	$delete_record = mysqli_query($connection, $query);
}
function GetUserPostsWhere($connection, $where)
{

}
function GetUserRealnameById($connection, $id)
{
	$query = "SELECT * FROM users WHERE user_id = {$id}";
	$query_result = mysqli_query($connection, $query);
	if(!$query_result)
	{
		die("Error " . mysqli_error($connection));
	}
	$row = mysqli_fetch_assoc($query_result);
	$username = $row['username'];
	$firstname = $row['user_firstname'];
	$lastname = $row['user_lastname'];
	$result = $firstname . " " . $lastname;
	
	return $result;
}
function GetUsernameById($connection, $id)
{
	$query = "SELECT * FROM users WHERE user_id = {$id}";
	$query_result = mysqli_query($connection, $query);
	if(!$query_result)
	{
		die("Error " . mysqli_error($connection));
	}
	$row = mysqli_fetch_assoc($query_result);
	$result = $row['username'];

	return $result;
}
function GetIdByUsername($connection, $id)
{
	$query = "SELECT * FROM users WHERE username = '{$id}'";
	$query_result = mysqli_query($connection, $query);
	if(!$query_result)
	{
		die("Error " . mysqli_error($connection));
	}
	$row = mysqli_fetch_assoc($query_result);
	$result = $row['user_id'];

	return $result;
}
function GetPostAuthorNameById($connection, $id)
{	
	$query = "SELECT u.user_firstname, u.user_lastname ";
	$query .= "FROM posts as p ";
	$query .= "INNER JOIN users as u ON u.user_id = p.post_author ";
	$query .= "WHERE p.post_author = {$id} ";
	$query_result = mysqli_query($connection, $query);
	if(!$query_result)
	{
		die("Error " . mysqli_error($connection));
	}
	$row = mysqli_fetch_assoc($query_result);
	$user_firstname = $row['user_firstname'];
	$user_lastname = $row['user_lastname'];
	$result = $user_firstname . " " . $user_lastname;

	return $result;
}
function IncreasePostViewsCount($connection, $id)
{
	$query = "UPDATE posts ";
	$query .= "SET post_views_count = post_views_count + 1 ";
	$query .= "WHERE post_id = {$id} ";
	$query_result = mysqli_query($connection, $query);
}
function ClonePost($connection, $id)
{
	$query = "SELECT * FROM posts WHERE post_id = {$id}";
	$query_result = mysqli_query($connection, $query);
	if(!$query_result)
	{
		die("Error " . mysqli_error($connection));
	}
	$row = mysqli_fetch_assoc($query_result);
	$post_category_id = $row['post_category_id'];
	$post_title = $row['post_title'];
	$post_author = $row['post_author'];
	$post_date = $row['post_date'];
	$post_image = $row['post_image'];
	$post_content = $row['post_content'];
	$post_tags = $row['post_tags'];
	$post_status = "unpublished";

	$query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status) ";
	$query .= "VALUES({$post_category_id}, '{$post_title}', '{$post_author}', now(), '{$post_image}', '{$post_content}', '{$post_tags}', '{$post_status}')";

	$create_post_query = mysqli_query($connection, $query);
}
function CheckUserExists($connection, $username)
{
	$query = "SELECT * FROM users WHERE username = '{$username}'";
	$query_result = mysqli_query($connection, $query);
	if(!$query_result)
	{
		die("Error " . mysqli_error($connection));
	}
	$result_count = mysqli_num_rows($query_result);
	
	if($result_count > 0)
	{
		return true;
	}
	return false;
	
}
function CheckEmailExists($connection, $email)
{
	$query = "SELECT * FROM users WHERE user_email = '{$email}'";
	$query_result = mysqli_query($connection, $query);
	if(!$query_result)
	{
		die("Error " . mysqli_error($connection));
	}
	$result_count = mysqli_num_rows($query_result);
	
	if($result_count > 0)
	{
		return true;
	}
	return false;
}
?>