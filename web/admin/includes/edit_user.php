<?php 
if(isset($_GET['uid']))
{
	$uid = CheckSql($_GET['uid']);
	
	$query = "SELECT * FROM users WHERE user_id = {$uid}";
	$select_user_by_id = ExecuteQuery($query);

	while($row = mysqli_fetch_assoc($select_user_by_id))
	{
		$user_id = $row['user_id'];
		$username = $row['username'];
		$user_firstname = $row['user_firstname'];
		$user_lastname = $row['user_lastname'];
		$user_email = $row['user_email'];
		$user_image = $row['user_image'];
		$user_role = $row['user_role'];
	}
}
if(isset($_POST['update_user']))
{
	$crypt_cost = 12;
	$username = CheckSql($_POST['username']);
	$user_password = CheckSql($_POST['user_password']);
	$user_firstname = CheckSql($_POST['user_firstname']);
	$user_lastname = CheckSql($_POST['user_lastname']);
	$user_email = CheckSql($_POST['user_email']);
	$user_image = $_FILES['user_image']['name'];
	$user_image_tmp = $_FILES['user_image']['tmp_name'];

	if(!empty($user_password))
	{
		$password_query = "SELECT user_password FROM users WHERE user_id = {$uid}";
		$get_user_query = ExecuteQuery($password_query);
		confirmQuery($connection, $get_user_query);

		$row = mysqli_fetch_assoc($get_user_query);
		$stored_user_password = $row['user_password'];
		// does nothing but waster DB server time for now
	}
	if($_POST['user_role'] !== 'Select Role')
	{
		$user_role = $_POST['user_role'];
	}
	$query = "UPDATE users SET ";
	$query .= "username = '{$username}', ";
	if(!empty($_POST['user_password'])) 
	{
		$user_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => $crypt_cost));
		$query .= "user_password = '{$user_password}', ";
	}
	$query .= "user_firstname = '{$user_firstname}', ";
	$query .= "user_lastname = '{$user_lastname}', ";
	$query .= "user_email = '{$user_email}', ";
	if(!empty($user_image))
	{
		$query .= "user_image = '{$user_image}', ";
	} else 
	{
		$queryImage = "SELECT * FROM users WHERE user_id = {$uid}";
		$select_user_by_id = ExecuteQuery($queryImage);
		$row = mysqli_fetch_assoc($select_user_by_id);
		$user_image = $row['user_image'];
	}
	$query .= "user_role = '{$user_role}' ";
	$query .= "WHERE user_id = {$uid}";

	global $connection;
	$update_user_query = ExecuteQuery($query);

	if(confirmQuery($update_user_query, $connection))
	{
		if($user_image)
		{
			move_uploaded_file($user_image_tmp, "../images/$user_image");
		}
		include "includes/messages/msg_user_update_success.php";
	}
	else 
	{
		include "includes/messages/msg_generic_error.php";
	}
} else 
{
	RedirectToIndex();
}


?>

<form action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" class="form-control" name="username" value="<?php echo $username ?>">
	</div>
	<div class="form-group">
		<label for="user_password">Password</label>
		<input type="password" class="form-control" name="user_password" placeholder="***************">
	</div>
	<div class="form-group">
		<label for="user_role">Role</label>
		<select name="user_role" id="" class="form-control">

			<?php 

			global $connection;
			$query = "SELECT * FROM roles";
			$select_roles = ExecuteQuery($query);

			confirmQuery($select_roles, $connection);

			while($row = mysqli_fetch_assoc($select_roles))
			{
				$role_id = $row['role_id'];
				$role_title = $row['role_title'];
				if($role_id == $user_role)
				{
					echo "<option selected value='$role_id'>$role_title</option>";
				} else
				{
					echo "<option value='$role_id'>$role_title</option>";
				}
			} 
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="user_email">Email Address</label>
		<input type="text" class="form-control" name="user_email" value="<?php echo $user_email ?>">
	</div>
	<div class="form-group">
		<label for="user_firstname">Firstname</label>
		<input type="text" class="form-control" name="user_firstname" value="<?php echo $user_firstname ?>">
	</div>
	<div class="form-group">
		<label for="user_lastname">Lastname</label>
		<input type="text" class="form-control" name="user_lastname" value="<?php echo $user_lastname ?>">
	</div>
	<?php if($user_image)
	{
		?>
		<div class="form-group">
			<img width="200" src="../images/<?php echo $user_image ?>">
		</div>
		<?php } ?>
		<div class="form-group">
			<label for="post_image">Upload a new image</label>
			<input type="file" class="form-control" name="user_image">
		</div>
		
		
		<div class="form-group">
			<input class="btn btn-primary" type="submit" class="form-control" name="update_user" value="Update User">
		</div>
	</form>