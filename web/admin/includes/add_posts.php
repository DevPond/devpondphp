<?php 
$post_author_name = "";
if(isset($_SESSION['user_id']))
{
	$user_id = $_SESSION['user_id'];
	$post_author_name = GetUsernameById($connection, $user_id);
}
if(isset($_POST['create_post']))
{
	global $connection;
	$post_title = CheckSql($_POST['post_title']);
	$post_author = CheckSql($_POST['post_author']);
	$post_author_id = CheckSql($_SESSION['user_id']);
	$post_category_id = CheckSql($_POST['post_category_id']);
	$post_status = CheckSql($_POST['post_status']);
	$post_image = $_FILES['post_image']['name'];
	$post_image_tmp = $_FILES['post_image']['tmp_name'];
	$post_tags = CheckSql($_POST['post_tags']);
	$post_content = CheckSql($_POST['post_content']);
	$post_date = date('d-m-y');

	$query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status) ";
	$query .= "VALUES({$post_category_id}, '{$post_title}', '{$post_author_id}', now(), '{$post_image}', '{$post_content}', '{$post_tags}', '{$post_status}')";

	$create_post_query = ExecuteQuery($query);

        if(!$create_post_query)
        {
        	include "includes/messages/msg_generic_error.php";
        }
        else
        {
        	move_uploaded_file($post_image_tmp, "../images/$post_image");
        	$post_id = mysqli_insert_id($connection);
        	include "includes/messages/msg_post_create_success.php";
        }
}

?>
<form action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="post_author">Post Author</label>
		<input type="text" class="form-control" name="post_author" id="author_readonly" value="<?php echo $post_author_name; ?>">
	</div>
	<div class="form-group">
		<label for="post_title">Post Title</label>
		<input type="text" class="form-control" name="post_title">
	</div>
	<div class="form-group">
		<label for="post_category_id">Post Category</label>
		<select name="post_category_id" id="" class="form-control">
			<?php 

			global $connection;
		            $query = "SELECT * FROM categories";
		            $select_categories = ExecuteQuery($query);

		            confirmQuery($select_categories, $connection);

		            while($row = mysqli_fetch_assoc($select_categories))
		        {
		            $cat_id = $row['cat_id'];
		            $cat_title = $row['cat_title'];

		            echo "<option value='$cat_id'>$cat_title</option>";
			} ?>
		</select>
	</div>
	<div class="form-group">
		<label for="post_status">Post Status</label>
		<select name="post_status" id="" class="form-control">
			<option value="draft">Select Option</option>
			<option value="draft">Draft</option>
			<option value="published">Publish</option>
		</select>
	</div>
	<div class="form-group">
		<label for="post_image">Image</label>
		<input type="file" class="form-control" name="post_image">
	</div>
	<div class="form-group">
		<label for="post_tags">Post Tags</label>
		<input type="text" class="form-control" name="post_tags">
	</div>
	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea class="form-control" id="" cols="30" rows="10" name="post_content">
		</textarea>
	</div>
	<div class="form-group">
		<input class="btn btn-primary" type="submit" class="form-control" name="create_post" value="Publish">
	</div>
</form>