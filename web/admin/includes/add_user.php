<?php 
if(isset($_POST['create_user']))
{
	$crypt_cost = 12;
	$username = CheckSql($_POST['username']);
	$user_password = CheckSql($_POST['user_password']);
	$user_lastname = CheckSql($_POST['user_lastname']);
	$user_firstname = CheckSql($_POST['user_firstname']);
	$user_email = CheckSql($_POST['user_email']);
	$user_image = $_FILES['user_image']['name'];
	$user_image_tmp = $_FILES['user_image']['tmp_name'];
	$user_role = CheckSql($_POST['user_role']);

	$user_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => $crypt_cost));

	$query = "INSERT INTO users(username, user_password, user_firstname, user_lastname, user_email, user_image, user_role) ";
	$query .= "VALUES('{$username}', '{$user_password}', '{$user_firstname}', '{$user_lastname}', '{$user_email}', '{$user_image}', '{$user_role}')";

	global $connection;
	$create_user_query = ExecuteQuery($query);

	if(!$create_user_query)
		{ ?>
	<?php include "includes/messages/msg_generic_error.php"; ?>
	<?php
	
}
else
{
	move_uploaded_file($user_image_tmp, "../images/$user_image");
	?>
	<?php include "includes/messages/msg_user_create_success.php"; ?>
	
	<?php } 
}

?>
<form action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" class="form-control" name="username">
	</div>
	<div class="form-group">
		<label for="user_password">Password</label>
		<input type="password" class="form-control" name="user_password">
	</div>
	<div class="form-group">
		<label for="user_role">Role</label>
		<select name="user_role" id="" class="form-control">

			<?php 

			global $connection;
			$query = "SELECT * FROM roles";
			$select_roles = ExecuteQuery($query);

			confirmQuery($select_roles, $connection);

			while($row = mysqli_fetch_assoc($select_roles))
			{
				$role_id = $row['role_id'];
				$role_title = $row['role_title'];

				echo "<option value='$role_id'>$role_title</option>";
			} 
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="user_email">Email Address</label>
		<input type="text" class="form-control" name="user_email">
	</div>
	<div class="form-group">
		<label for="user_firstname">Firstname</label>
		<input type="text" class="form-control" name="user_firstname">
	</div>
	<div class="form-group">
		<label for="user_lastname">Lastname</label>
		<input type="text" class="form-control" name="user_lastname">
	</div>
	<div class="form-group">
		<label for="user_image">Image</label>
		<input type="file" class="form-control" name="user_image">
	</div>


	<div class="form-group">
		<input class="btn btn-primary" type="submit" class="form-control" name="create_user" value="Add User">
	</div>
</form>