<?php
include "modal_delete.php";
if(isset($_POST['checkBoxArray']))
{
    foreach ($_POST['checkBoxArray'] as $cbValue) {
        $bulkOptions = $_POST['bulk_options'];
        $cbValue = CheckSql($cbValue);
        switch ($bulkOptions) {
            case 'published':
            $query = "UPDATE posts SET post_status = 'published' WHERE post_id = {$cbValue}";
            $update_post = ExecuteQuery($query);
            break;
            case 'draft':
            $query = "UPDATE posts SET post_status = 'draft' WHERE post_id = {$cbValue}";
            $update_post = ExecuteQuery($query);
            break;
            case 'clone':
            ClonePost($connection, $cbValue);
            break;
            case 'delete':
            DeleteRecordsWhere($connection, "posts", "post_id = {$cbValue}" );
            break;
            
            default:
                // code...
            break;
        }
        
    }
}

?>
<form action="" method="post">
    <table class="table table-bordered table-hover">
        <div id="bulkOptionContainer" class="col-xs-4">
            <label for="bulk_options">Post Options:</label>
            <select name="bulk_options" class="form-control">
                <option value="">Select Options</option>
                <option value="published">Publish</option>
                <option value="draft">Draft</option>
                <option value="clone">Clone</option>
                <option value="delete">Delete</option>
            </select>

        </div>
        <div class="col-xs-2">
            <input type="submit" name="submit" class="btn btn-success" value="Apply">
            <a class="btn btn-primary" href="posts.php?source=add_post">Add New</a>
        </div>
        
        <div id="sortOptionContainer" class="col-xs-2">
            <label for="sort_options">Sort By:</label>
            <select name="sort_options" class="form-control">
                <option value="">Select Options</option>
                <option value="sort_date">Post Date</option>
                <option value="sort_author">Author</option>
                <option value="sort_views">Views</option>
                <option value="sort_status">Status</option>
            </select>
        </div>
        <div class="col-xs-2">
            <input type="submit" name="submit_sort" class="btn btn-success" value="Sort">
        </div>

        <thead>
            <tr>
                <th><input id='selectAll' type='checkbox'></th>
                <th>Id</th>
                <th>Author</th>
                <th>Title</th>
                <th>Views</th>
                <th>Category</th>
                <th>Status</th>
                <th>Image</th>
                <th>Tags</th>
                <th>Comments</th>
                <th>Date</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $sort_order = "post_id DESC"; // default sort
            if(isset($_POST['submit_sort']))
            {
                $sort_order_selected = $_POST['sort_options'];
                switch ($sort_order_selected) {
                    case 'sort_date':
                    $sort_order = 'post_date DESC';
                    break;
                    case 'sort_views':
                    $sort_order = 'post_views_count DESC';
                    break;
                    case 'sort_author':
                    $sort_order = 'post_author ASC';
                    break;
                    case 'sort_date':
                    $sort_status = 'post_status ASC';
                    break;
                    
                    default:
                    $sort_order = "post_id DESC";
                    break;
                }
            }
            $query = "SELECT ";
            $query .= "po.post_id, ";
            $query .= "po.post_author, ";
            $query .= "po.post_content, ";
            $query .= "po.post_category_id, ";
            $query .= "po.post_date, ";
            $query .= "po.post_image, ";
            $query .= "po.post_status, ";
            $query .= "po.post_tags, ";
            $query .= "po.post_title, ";
            $query .= "po.post_views_count, ";
            $query .= "ca.cat_id, ";
            $query .= "ca.cat_title ";

            $query .= "FROM posts as po "; 
            $query .= "INNER JOIN categories as ca ON po.post_category_id = ca.cat_id ";

            $query .= "ORDER BY $sort_order ";

            $select_posts = ExecuteQuery($query);

            while($row = mysqli_fetch_assoc($select_posts))
            {
                $post_id = $row['post_id'];
                $post_author = $row['post_author'];
                $post_category_id = $row['post_category_id'];
                $post_content = $row['post_content'];
                $post_date = $row['post_date'];
                $post_image = $row['post_image'];
                $post_status = $row['post_status'];
                $post_tags = $row['post_tags'];
                $post_title = $row['post_title'];
                $post_views = $row['post_views_count'];
                $cat_id = $row['cat_id']; 
                $cat_title = $row['cat_title'];

                $query_comment_count = "SELECT * FROM comments WHERE comment_post_id = $post_id ";
                $select_comment_count = ExecuteQuery($query_comment_count);
                $post_comment_count = mysqli_num_rows($select_comment_count);
                $row_comment = mysqli_fetch_assoc($select_comment_count);
                $comment_id = $row_comment['comment_id'];

                $Authorname = GetUserRealnameById($connection, $post_author);
                
                echo "<tr>";
                echo "<td><input class='checkBoxes' type='checkbox' name='checkBoxArray[]' value='{$post_id}'</td>";
                echo "<td>{$post_id}</td>";
                echo "<td>{$Authorname}</td>";
                echo "<td><a href='../post.php?pid={$post_id}'>{$post_title}</a></td>";
                echo "<td>{$post_views}</td>";
                echo "<td>{$cat_title}</td>";
                echo "<td>{$post_status}</td>";
                echo "<td><img src='../images/{$post_image}' width='300'></td>";
                echo "<td>{$post_tags}</td>";
                echo "<td><a href='comments.php?postid={$post_id}'>{$post_comment_count}</a></td>";
                echo "<td>{$post_date}</td>";
                echo "<td><a href='posts.php?source=edit_post&pid={$post_id}'>Edit</a></td>";
                echo "<td><a href='#' rel='{$post_id}' class='modal_delete'>Delete</a></td>";
                
                echo "</tr>";
            }
            ?>

        </tbody>
    </table>
</form>

<?php 

if(isset($_GET['delete']))
{
    if(isset($_SESSION['user_role']))
    {
        if($_SESSION['user_role'] == 1) {
            $recordId = CheckSql($_GET['delete']);
            deleteRecord($connection, 'post', $recordId);
            header("Location: posts.php");
        }
    } else {
        header("Location: index.php");
    }
}
?>

<script type="text/javascript">
$(document).ready(function(){

    $(".modal_delete").on('click', function(){
        var postId = $(this).attr("rel");
        var delete_url = "posts.php?delete=" + postId;
        $(".modal_delete_link").attr("href", delete_url);
        $("#myModal").modal('show');
    })
});
</script>