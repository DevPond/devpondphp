<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Image</th>
            <th>Role</th>
            <th>Make Admin</th>
            <th>Make User</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $query = GetUsersWithRoles();

        $select_users = ExecuteQuery($query);

        while($row = mysqli_fetch_assoc($select_users))
        {
            $user_id = $row['user_id'];
            $username = $row['username'];
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $user_email = $row['user_email'];
            $user_image = $row['user_image'];
            $user_role = $row['role_title'];


            echo "<tr>";
            echo "<td>{$user_id}</td>";
            echo "<td>{$username}</td>";
            echo "<td>{$user_firstname}</td>";
            echo "<td>{$user_lastname}</td>";
            echo "<td>{$user_email}</td>";
            echo "<td>{$user_image}</td>";
            echo "<td>{$user_role}</td>";
            echo "<td><a href='users.php?change_to_admin={$user_id}'>Make Admin</a></td>";
            echo "<td><a href='users.php?change_to_user={$user_id}'>Make User</a></td>";
            echo "<td><a href='users.php?source=edit&uid={$user_id}'>Edit</a></td>";
            echo "<td><a href='users.php?delete={$user_id}'>Delete</a></td>";
            echo "</tr>";
        }
        ?>

    </tbody>
</table>

<?php 

if(isset($_GET['delete']))
{
    if(isset($_SESSION['user_role']))
    {
        if($_SESSION['user_role'] == 1) {
            $recordId = CheckSql($_GET['delete']);
            deleteRecord($connection, 'user', $recordId);
            header("Location: users.php");
        }
    } else {
        header("Location: ../index.php");
    }

}
if(isset($_GET['change_to_admin']))
{ 
    if(isset($_SESSION['user_role']))
    {
        if($_SESSION['user_role'] == 1) {
            $the_user_id = CheckSql($_GET['change_to_admin']);
            $query = "UPDATE users SET user_role = 1 WHERE user_id = {$the_user_id}";
            $change_query = ExecuteQuery($query);
            header("Location: users.php");
        } else {
            header("Location: ../index.php");
        }
    }
    if(isset($_GET['change_to_user']))
    {
        if(isset($_SESSION['user_role']))
        {
            if($_SESSION['user_role'] == 1) {
                $the_user_id = CheckSql($_GET['change_to_user']);
                $query = "UPDATE users SET user_role = 2 WHERE user_id = {$the_user_id}";
                $change_query = ExecuteQuery($query);
                header("Location: users.php");
            } else {
                header("Location: ../index.php");
            }
        }
    }
}
?>