<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Post</th>
            <th>Author</th>
            <th>Email</th>
            <th>Status</th>
            <th>Content</th>
            <th>Date</th>
            <th>Approve</th>
            <th>Unapprove</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $query = "SELECT * FROM comments  ";
        if(isset($_GET['postid']))
        {
            $post_id = CheckSql($_GET['postid']);
            $query .= "WHERE comment_post_id = {$post_id} ";
        }
        $query .= "ORDER BY comment_id DESC ";
        $select_posts = ExecuteQuery($query);

        while($row = mysqli_fetch_assoc($select_posts))
        {
            $comment_id = $row['comment_id'];
            $comment_post_id = $row['comment_post_id'];
            $comment_author = $row['comment_author'];
            $comment_email = $row['comment_email'];
            $comment_content = $row['comment_content'];
            $comment_status = $row['comment_status'];
            $comment_date = $row['comment_date'];

            $queryPostIds = "SELECT * FROM posts WHERE post_id = {$comment_post_id}";
            $select_comments_id = ExecuteQuery($queryPostIds);
            $rowPost = mysqli_fetch_assoc($select_comments_id);
            $post_id = $rowPost['post_id']; 
            $post_title = $rowPost['post_title'];


            echo "<tr>";
            echo "<td>{$comment_id}</td>";
            echo "<td><a href='../post.php?pid={$post_id}'>{$post_title}</a></td>";
            echo "<td>{$comment_author}</td>";
            echo "<td>{$comment_email}</td>";
            echo "<td>{$comment_status}</td>";
            echo "<td>{$comment_content}</td>";
            echo "<td>{$comment_date}</td>";
            
            if(isset($_GET['postid']))
            {
                echo "<td><a href='comments.php?approve=1&cid={$comment_id}&postid={$post_id}'>Approve</a></td>";
                echo "<td><a href='comments.php?approve=0&cid={$comment_id}&postid={$post_id}'>Unapprove</a></td>";
                echo "<td><a href='comments.php?delete={$comment_id}&postid={$post_id}'>Delete</a></td>";
            } else {
                echo "<td><a href='comments.php?approve=1&cid={$comment_id}'>Approve</a></td>";
                echo "<td><a href='comments.php?approve=0&cid={$comment_id}'>Unapprove</a></td>";
                echo "<td><a href='comments.php?delete={$comment_id}'>Delete</a></td>";
            }
            echo "</tr>";
        }
        ?>

    </tbody>
</table>

<?php 

if(isset($_GET['delete']))
{
    if(isset($_SESSION['user_role']))
    {
        if($_SESSION['user_role'] == 1) {
            $recordId = CheckSql($_GET['delete']);
            deleteRecord($connection, 'comment', $recordId);
            if(isset($_GET['postid']))
            {
                header("Location: comments.php?postid={$_GET['postid']}");
            } else {
                header("Location: comments.php");
            }
        }
    } else {
        header("Location: ../index.php");
    }

}
if(isset($_GET['approve']))
{
    if(isset($_SESSION['user_role']))
    {
        if($_SESSION['user_role'] == 1) {
            $approve = CheckSql($_GET['approve']);
            $the_comment_id = CheckSql($_GET['cid']);
            $query = "";
            if($approve == 0)
            {
                $query = "UPDATE comments SET ";
                $query .= "comment_status = 'Unapproved' ";
                $query .= "WHERE comment_id = {$the_comment_id}";
            } else 
            {
                $query = "UPDATE comments SET ";
                $query .= "comment_status = 'Approved' ";
                $query .= "WHERE comment_id = {$the_comment_id}";
            }
            $approveQuery = ExecuteQuery($query);
            if(isset($_GET['postid']))
            {
                header("Location: comments.php?postid={$_GET['postid']}");
            } else {
                header("Location: comments.php");
            }
        }
    } else {
        header("Location: ../index.php");
    }
}
?>