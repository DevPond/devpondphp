<?php 
if(isset($_GET['pid']))
{
	$pid = CheckSql($_GET['pid']);

	$query = "SELECT * FROM posts WHERE post_id = {$pid}";
	$select_post_by_id = ExecuteQuery($query);

	while($row = mysqli_fetch_assoc($select_post_by_id))
	{
		$post_id = $row['post_id'];
		$post_author = $row['post_author'];
		$post_category_id = $row['post_category_id'];
		$post_comment_count = $row['post_comment_count'];
		$post_content = $row['post_content'];
		$post_date = $row['post_date'];
		$post_image = $row['post_image'];
		$post_status = $row['post_status'];
		$post_tags = $row['post_tags'];
		$post_title = $row['post_title'];
		$post_author_name = GetUsernameById($connection, $post_author);
	}
}
if(isset($_POST['update_post']))
{
	$post_title = CheckSql($_POST['post_title']);
	$post_author = GetIdByUsername($connection, CheckSql($_POST['post_author']));
	$post_category_id = CheckSql($_POST['post_category_id']);
	$post_status = CheckSql($_POST['post_status']);
	$post_image = $_FILES['post_image']['name'];
	$post_image_tmp = $_FILES['post_image']['tmp_name'];
	$post_tags = CheckSql($_POST['post_tags']);
	$post_content = CheckSql($_POST['post_content']);
	if(isset($_POST['cb_reset_views']))
	{
		$post_reset_views = $_POST['cb_reset_views'];
	} else {
		$post_reset_views = false;
	}


	$query = "UPDATE posts SET ";
	$query .= "post_title = '{$post_title}', ";
	$query .= "post_category_id = {$post_category_id}, ";
	$query .= "post_author = '{$post_author}', ";
	$query .= "post_status = '{$post_status}', ";
	$query .= "post_tags = '{$post_tags}', ";
	if(!empty($post_image))
	{
		$query .= "post_image = '{$post_image}', ";

		
	} else 
	{
		$queryImage = "SELECT * FROM posts WHERE post_id = {$pid}";
		$select_post_by_id = ExecuteQuery($queryImage);
		$row = mysqli_fetch_assoc($select_post_by_id);
		$post_image = $row['post_image'];
	}
	if($post_reset_views == true)
	{
		$query .= "post_views_count = 0, ";
	}
	$query .= "post_content = '{$post_content}' ";
	$query .= "WHERE post_id = {$pid}";

	global $connection;
	$update_post_query = ExecuteQuery($query);

	if(confirmQuery($update_post_query, $connection))
	{
		if($post_image)
		{
			move_uploaded_file($post_image_tmp, "../images/$post_image");
		}
		include "includes/messages/msg_post_update_success.php";
	}	else
	{
		include "includes/messages/msg_generic_error.php";
	}
}


?>

<form action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="post_author">Post Author</label>
		<input value="<?php echo $post_author_name; ?>" id="author_readonly" type="text" class="form-control" name="post_author">
	</div>
	<div class="form-group">
		<label for="post_title">Post Title</label>
		<input value="<?php echo $post_title; ?>" type="text" class="form-control" name="post_title">
	</div>
	<div class="form-group">
		<label for="post_category_id">Post Category</label>
		<select name="post_category_id" id="" class="form-control">
			<?php 

			global $connection;
			$query = "SELECT * FROM categories";
			$select_categories = ExecuteQuery($query);

			confirmQuery($select_categories, $connection);

			while($row = mysqli_fetch_assoc($select_categories))
			{
				$cat_id = $row['cat_id'];
				$cat_title = $row['cat_title'];
				if($cat_id == $post_category_id)
				{
					echo "<option selected value='$cat_id'>$cat_title</option>";
				} else {
					echo "<option value='$cat_id'>$cat_title</option>";
				}

				
			} ?>
		</select>
	</div>
	<div class="form-group">
		<label for="post_status">Post Status</label>
		<select name="post_status" id="" class="form-control">
			<option value="<?php echo $post_status ?>"><?php echo $post_status ?></option>
			<?php 
			if($post_status == "published")
			{
				echo "<option value='draft'>Draft</option>";
			} else {
				echo "<option value='published'>Published</option>";
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<img width="300" src="../images/<?php echo $post_image ?>">

	</div>
	<div class="form-group">
		<label for="post_image">Upload a new image</label>
		<input type="file" class="form-control" name="post_image">
	</div>
	<div class="form-group">
		<label for="post_tags">Post Tags</label>
		<input value="<?php echo $post_tags; ?>" type="text" class="form-control" name="post_tags">
	</div>
	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea class="form-control" id="" cols="30" rows="10" name="post_content">
			<?php echo str_replace('\r\n', '<br />', $post_content); ?>
		</textarea>
	</div>
	<div class="form-group">
		<input type="checkbox" name="cb_reset_views"></input>
		<label for="cb_reset_views">Reset views count?</label>
	</div>
	<div class="form-group">
		<input class="btn btn-primary" type="submit" class="form-control" name="update_post" value="Publish">
	</div>
</form>