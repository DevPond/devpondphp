<div class="alert alert-danger">
	<strong>Error!</strong> There was an error processing this request. The message was:
	<br />
	<?php echo mysqli_error($connection) ?>
</div>