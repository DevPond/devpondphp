<?php include "includes/admin_header.php" ?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include "includes/admin_navigation.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <?php include "includes/page_header.php" ?>
                        
                        <?php
                        $source = '';
                        if(isset($_GET['source']))
                        {
                            $source = $_GET['source'];
                        }

                        switch ($source) {
                            case 'add_post':
                                include "includes/add_posts.php";
                                break;
                            case 'edit_post':
                                include "includes/edit_post.php";
                                break;
                            
                            default:
                                include "includes/view_all_posts.php";
                                break;
                        }

                        ?>
                    
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include "includes/admin_footer.php" ?>

