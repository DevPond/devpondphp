<?php include "includes/admin_header.php" ?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include "includes/admin_navigation.php" ?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <?php include "includes/page_header.php" ?>
            <!-- /.row -->

            <!-- /.row -->
            <!-- Widgets -->
            <?php include "includes/admin_widgets.php" ?>
            <!-- /.row -->
            <?php
            $draftCount = GetRecordsCountWhere($connection, "posts", "post_status = 'draft'");
            $unapprovedCommentsCount = GetRecordsCountWhere($connection, "comments", "comment_status = 'unapproved'");
            $standardUserCount = GetRecordsCountWhere($connection, "users", "user_role = 2");
            $publishedPostsCount = GetRecordsCountWhere($connection, "posts", "post_status = 'published'");
            ?>
            <div class="row">
                <?php 
                $element_text = ['Published Posts','Active Posts', 'Draft Posts', 'Comments', 'Pending Comments', 'Users', 'Standard Users', 'Categories'];
                $element_count = [$publishedPostsCount, $postCount, $draftCount, $commentCount, $unapprovedCommentsCount, $userCount, $standardUserCount, $catCount];
                ?>
                <script type="text/javascript">
                google.charts.load('current', {'packages':['bar']});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                      ['Data', 'Count'],

                      <?php 
                      $array_length = count($element_text);
                      for($i = 0; $i < $array_length; $i++)
                      {
                        echo "['{$element_text[$i]}', {$element_count[$i]}]";
                        if ($i != $array_length - 1)
                        {
                            echo ",";
                        }
                    } 
                    ?>
                    ]);

                    var options = {
                      chart: {
                        title: '',
                        subtitle: '',
                    }
                };

                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                chart.draw(data, options);
            }
            </script>
            <div id="columnchart_material" style="width: 'auto'; height: 500px;"></div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->


<?php include "includes/admin_footer.php" ?>

