<?php session_start(); ?>
<?php include "includes/header.php" ?>
<?php include "includes/db.php" ?>
<?php include "admin/includes/sql_statements.php" ?>
<?php include "admin/functions.php" ?>
<body>

    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <?php
                if(isset($_GET['id']))
                {

                    $post_author_name = GetPostAuthorNameById($connection, CheckSql($_GET['id']));
                }
                ?>
                <h1 class="page-header">
                    All posts by
                    <small><?php echo $post_author_name; ?></small>
                </h1>
                <?php
                if(isset($_GET['id']))
                {
                    $author_id = CheckSql($_GET['id']);
                }
                $query = "SELECT * FROM posts WHERE post_author = '$author_id'";
                $select_all_posts = mysqli_query($connection, $query);

                while($row = mysqli_fetch_assoc($select_all_posts))
                {
                    $post_title = $row['post_title'];
                    $post_author = $row['post_author'];
                    $post_date = $row['post_date'];
                    $post_image = $row['post_image'];
                    $post_content = $row['post_content'];
                    $post_id = $row['post_id'];
                    $post_author_name = GetUserRealnameById($connection, $post_author);

                    ?>


                    <?php 
                    if(isset($_SESSION['user_role']))
                    {
                        if($_SESSION['user_role'] == 1)
                        {

                            ?>
                            <div class="pull-right">
                                <small class="text-right" ><a href="admin/posts.php?source=edit_post&pid=<?php echo $post_id ?>">Edit this post</a></small>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    

                    <!-- Blog Post -->
                    <h2>
                        <a href="post.php?pid=<?php echo $post_id ?>"><?php echo $post_title ?></a>
                    </h2>
                    <p class="lead">
                        by <a href="user_posts.php?id=<?php echo $post_author ?>"><?php echo $post_author_name ?></a>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date ?></p>
                    <hr>
                    <img class="img-responsive" src="images/<?php echo $post_image ?>" alt="">
                    <hr>
                    <p><?php echo $post_content ?></p>
                    
                    <hr>
                    
                    <?php } ?>

                    <!-- Blog Comments -->

                    <?php 
                    if(isset($_POST['create_comment']))
                    {
                        $comment_author = $_POST['comment_author'];
                        $comment_email = $_POST['comment_email'];
                        $comment_content = $_POST['comment_content'];
                        $comment_status = "Unapproved";

                        if(!empty($comment_author) && (!empty($comment_email)) && (!empty($comment_content))) {
                            $query = "INSERT INTO comments (comment_author, comment_content, comment_date, comment_email, comment_post_id, comment_status) ";
                            $query .= "VALUES ('{$comment_author}', '{$comment_content}', now(), '{$comment_email}', {$post_id}, '{$comment_status}')";

                            global $connection;
                            $create_comment_query = mysqli_query($connection, $query);

                            if(!$create_comment_query)
                            {
                                die("Error" . mysqli_error($connection));
                            }

                            $query = "UPDATE posts SET post_comment_count = post_comment_count + 1 ";
                            $query .= "WHERE post_id = {$post_id} ";
                            $update_comment_count_query = mysqli_query($connection, $query);

                            if(!$update_comment_count_query)
                            {
                                die("Error" . mysqli_error($connection));
                            }
                        } else 
                        {
                            echo "<script>alert('Fields can not be empty')</script>";
                        }
                    }

                    ?>

                    <!-- Comments Form -->
                    

                    <!-- Posted Comments -->
                    
                    <!-- Comment -->

                </div>

                <!-- Blog Sidebar Widgets Column -->
                <?php include "includes/sidebar.php" ?>

            </div>
            <!-- /.row -->

            <?php include "includes/footer.php" ?>