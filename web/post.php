<?php session_start(); ?>
<?php include "includes/header.php" ?>
<?php include "includes/db.php" ?>
<?php include "admin/includes/sql_statements.php" ?>
<?php include "admin/functions.php" ?>
<body>

    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <?php
                $showPost = false;
                if(isset($_GET['pid']))
                {
                    $post_id = CheckSql($_GET['pid']);
                    
                    $query = "SELECT * FROM posts WHERE post_id = $post_id ";
                    if(!SessionUserIsAdmin())
                    {
                        $query .= "AND post_status = 'published'";
                    }
                    $select_all_posts = mysqli_query($connection, $query);

                    while($row = mysqli_fetch_assoc($select_all_posts))
                    {
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_image = $row['post_image'];
                        $post_content = $row['post_content'];
                        $post_author_name = GetUserRealnameById($connection, $post_author);

                        IncreasePostViewsCount($connection, $post_id);
                        $showPost = true;

                        ?>
                        
                        <h1 class="page-header">
                            Page Heading
                            <small>Secondary Text</small>
                            <?php 
                            if(SessionUserIsAdmin())
                            {
                                ?>
                                <div class="pull-right">
                                    <small class="text-right" ><a href="admin/posts.php?source=edit_post&pid=<?php echo $post_id ?>">Edit this post</a></small>
                                </div>
                                <?php
                                
                            }
                            ?>
                        </h1>

                        <!-- Blog Post -->
                        <h2>
                            <a href="post.php?pid=<?php echo $post_id ?>"><?php echo $post_title ?></a>
                        </h2>
                        <p class="lead">
                            by <a href="user_posts.php?id=<?php echo $post_author ?>"><?php echo $post_author_name ?></a>
                        </p>
                        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date ?></p>
                        <hr>
                        <img class="img-responsive" src="images/<?php echo $post_image ?>" alt="">
                        <hr>
                        <p><?php echo $post_content ?></p>
                        
                        <hr>
                        
                        <?php 
                    }  
                    ?>

                    <!-- Blog Comments -->

                    <?php 
                    if($showPost)
                    {
                        if(isset($_POST['create_comment']))
                        {
                            $comment_author = $_POST['comment_author'];
                            $comment_email = $_POST['comment_email'];
                            $comment_content = $_POST['comment_content'];
                            $comment_status = "Unapproved";

                            if(!empty($comment_author) && (!empty($comment_email)) && (!empty($comment_content))) {
                                $query = "INSERT INTO comments (comment_author, comment_content, comment_date, comment_email, comment_post_id, comment_status) ";
                                $query .= "VALUES ('{$comment_author}', '{$comment_content}', now(), '{$comment_email}', {$post_id}, '{$comment_status}')";

                                global $connection;
                                $create_comment_query = mysqli_query($connection, $query);

                                if(!$create_comment_query)
                                {
                                    die("Error" . mysqli_error($connection));
                                }
                                $query .= "WHERE post_id = {$post_id} ";
                                $update_comment_count_query = mysqli_query($connection, $query);

                                if(!$update_comment_count_query)
                                {
                                    die("Error" . mysqli_error($connection));
                                }
                            } else 
                            {
                                echo "<script>alert('Fields can not be empty')</script>";
                            }
                        }
                        ?>
                        <!-- Comments Form -->
                        <div class="well">
                            <h4>Leave a Comment:</h4>
                            <form role="form" action="" method="post">
                                <div class="form-group">
                                    <label for="comment_author">Name</label>
                                    <input type="text" name="comment_author" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="comment_author">Email</label>
                                    <input type="email" name="comment_email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="comment_content">Message</label>
                                    <textarea class="form-control" rows="3" name="comment_content"></textarea>
                                </div>
                                <button type="submit" name="create_comment" class="btn btn-primary">Submit</button>
                            </form>
                        </div>

                        <hr>
                        <?php 
                    }
                    ?>
                    <!-- Posted Comments -->
                    <?php 
                    if($showPost)
                    {
                        $query = "SELECT * FROM comments WHERE comment_post_id = $post_id AND comment_status='approved' ";
                        $query .= "ORDER BY comment_id DESC";

                        $select_all_comments = mysqli_query($connection, $query);

                        while($row = mysqli_fetch_assoc($select_all_comments))
                        {
                            $comment_author = $row['comment_author'];
                            $comment_date = $row['comment_date'];
                            $comment_content = $row['comment_content'];

                            ?>
                            <!-- Comment -->
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><?php echo $comment_author ?>
                                        <small><?php echo $comment_date ?></small>
                                    </h4>
                                    <?php echo $comment_content ?>
                                </div>
                            </div>

                            <?php 
                        } 

                        ?>

                        <!-- Comment -->
                        <div class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="http://placehold.it/64x64" alt="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Start Bootstrap
                                    <small>August 25, 2014 at 9:30 PM</small>
                                </h4>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                <!-- Nested Comment -->
                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">Nested Start Bootstrap
                                            <small>August 25, 2014 at 9:30 PM</small>
                                        </h4>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </div>
                                </div>
                                <!-- End Nested Comment -->
                            </div>
                        </div>
                        <?php 
                    } else {
                        header("Location: index.php");
                    }
                }
                ?>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>

        <?php include "includes/footer.php" ?>