<?php session_start() ?>
<?php include "includes/header.php" ?>
<?php include "includes/db.php" ?>
<?php include "admin/includes/sql_statements.php" ?>
<?php include "admin/functions.php" ?>
<body>

    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>
                <?php

                $page = "";
                if(isset($_GET['page']))
                {
                    $page = CheckSql($_GET['page']);
                }
                if($page == "" || $page == 1)
                {
                    $selected_page = 0;
                }
                else {
                    $selected_page = ($page * 5) - 5;
                }
                $post_count_query = "SELECT * FROM posts ";
                if(!SessionUserIsAdmin()){
                    $post_count_query .= "WHERE post_status = 'published'";
                }
                $find_count = mysqli_query($connection, $post_count_query);
                $row_count = mysqli_num_rows($find_count);
                $row_count = ceil($row_count / 5);

                $query = "SELECT * FROM posts ";
                if(!SessionUserIsAdmin()){
                    $query .= "WHERE post_status = 'published'";
                }
                $query .= "ORDER BY post_id DESC "; //change to datetime when sql columns change
                $query .= "LIMIT {$selected_page}, 5 ";

                $select_all_posts = mysqli_query($connection, $query);
                if(mysqli_num_rows($select_all_posts) > 0)
                {
                    while($row = mysqli_fetch_assoc($select_all_posts))
                    {
                        $post_id = $row['post_id'];
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_image = $row['post_image'];
                        $post_content = substr($row['post_content'], 0, 200);
                        $post_status = $row['post_status'];

                        $post_author_name = GetUserRealnameById($connection, $post_author);

                        ?>
                        
                        <!-- First Blog Post -->
                        <h2>
                            <a href="post.php?pid=<?php echo $post_id ?>"><?php echo $post_title ?></a>
                        </h2>
                        <p class="lead">
                            by <a href="user_posts.php?id=<?php echo $post_author ?>"><?php echo $post_author_name ?></a>
                        </p>
                        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date ?></p>
                        <hr>
                        <a href="post.php?pid=<?php echo $post_id ?>">
                            <img class="img-responsive" src="images/<?php echo $post_image ?>" alt="">
                        </a>
                        <hr>
                        <p><?php echo $post_content ?></p>
                        <a class="btn btn-primary" href="post.php?pid=<?php echo $post_id ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                        <hr>
                        
                        <?php  } }
                        else { echo "<h2> There are currently no posts on the site. </H2>";
                    }
                    ?>


                </div>

                <!-- Blog Sidebar Widgets Column -->
                <?php include "includes/sidebar.php" ?>

            </div>
            <!-- /.row -->

            <hr>
            <ul class="pagination">
                <?php 
                for($i = 1; $i <= $row_count; $i++)
                {
                    if($i == $page){
                        echo "<li class='page-item disabled'><a href='#'>{$i}</a></li>";
                    } else {
                        echo "<li class='page-item'><a href='index.php?page={$i}'>{$i}</a></li>";
                    }
                }
                ?>
            </ul>

            <?php include "includes/footer.php" ?>