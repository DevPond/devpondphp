<?php session_start(); ?>
<?php  include "includes/db.php"; ?>
<?php  include "includes/header.php"; ?>
<?php include "admin/includes/sql_statements.php"; ?>
<?php include "admin/functions.php" ?>
<?php 

$isSuccess = false;
if(isset($_POST['submit']))
{
    if(!empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['password']))
    {
        global $connection;
        if(!CheckUserExists($connection, $_POST['username']) || !CheckEmailExists($connection, $_POST['email']))
        {
            $reg_user_role = 2;
            $crypt_cost = 12;
            
            $reg_username = CheckSql($_POST['username']);
            $reg_email = CheckSql($_POST['email']);
            $reg_password = CheckSql($_POST['password']);

            $reg_password = password_hash($reg_password, PASSWORD_BCRYPT, array('cost' => $crypt_cost));

            $query = "INSERT INTO users ";
            $query .= "(username, user_email, user_password, user_role) ";
            $query .= "VALUES ";
            $query .= "('{$reg_username}', '{$reg_email}', '{$reg_password}', {$reg_user_role}) ";

            $result = mysqli_query($connection, $query);

            if(!$result)
            {
                die("Error " . mysqli_error($connection));
            }
            $isSuccess = true;
        }
    } 
    
}
?>


<!-- Navigation -->

<?php  include "includes/navigation.php"; ?>


<!-- Page Content -->
<div class="container">

    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="form-wrap">
                        <h1>Register</h1>
                        <form role="form" action="registration.php" method="post" id="login-form" autocomplete="off">
                            <div class="form-group">
                                <label for="username" class="sr-only">username</label>
                                <input type="text" required name="username" id="username" class="form-control" placeholder="Enter Desired Username">
                            </div>
                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="email" required name="email" id="email" class="form-control" placeholder="somebody@example.com">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" required name="password" id="key" class="form-control" placeholder="Password">
                            </div>

                            <input type="submit" name="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Register">
                        </form>
                        <?php 
                        if(isset($_POST['submit']))
                        {
                            if($isSuccess) 
                            { 
                                include "includes/messages/msg_user_create_success.php"; 
                            } 
                            else {
                                include "includes/messages/msg_user_create_failed.php";
                            }
                        } 
                        
                        ?>
                    </div>
                </div> <!-- /.col-xs-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>


    <hr>



    <?php include "includes/footer.php";?>
